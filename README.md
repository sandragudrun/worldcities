# Heimsborgir

Á þessari síðu er hægt að lesa sér til og fræðast um höfuðborgir heimsins. 

## Getting Started

To start contributing to this project you should fork it to your own GitLab repository. Then you can clone it to your local repository on your machine.

### Prerequisites

You will need to have Node.js on your machine and this document will assume that you have yarn (npm should be ok too). To install yarn do:
npm install -g yarn
you will also need to have jest (that is the testing framework: https://jestjs.io/docs/en/getting-started). To install it just run the yarn command in the terminal in your project directory with no arguments like so:
 yarn

### How to contribute to the project 
To add a city to the project you will have to edit the main.js file by adding text to the array.<br>
First add the name of the city to title. Second, add an image that describes the city. You have to save you image in the img folder to reference it in the array. Third, add your text. <br>
*Remember! You have to put a comma after the last curly brackets before adding your curly brackets. <br>
Those three points are a requirement for passing the tests - a headline, an image and a text. 

### Opening the page in your browser
 Once you fork this library to your own GitLab a CI process will start runing and after it has passed you should be able to run the page under settings > pages

## Running the tests
Before you commit your code and do a merge request you should test your code by typing into terminal

```yarn test```
